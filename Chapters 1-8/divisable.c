#include <stdio.h>

int main(void){
    
    int value1, value2;
    
    printf("Enter two numbers to check if divisable: \n");
    scanf("%d%d", &value1, &value2);
    
    if(value1 % value2 == 0){
        printf("Is divisable.\n");
    }else{
        printf("Not divisable.\n");
    }
    
    return 0;
}