#include <stdio.h>

int main(void){
    
    int i1, i2, i3, j1, j2, j3, next_multiple1, next_multiple2, next_multiple3;
    
    i1 = 365;
    i2 = 12258;
    i3 = 996;
    
    j1 = 7;
    j2 = 23;
    j3 = 4;
  
    next_multiple1 = i1 + j1 - (i1 % j1);
    next_multiple2 = i2 + j2 - (i2 % j2);
    next_multiple3 = i3 + j3 - (i3 % j3);
    
    printf("The values are: \n%i\n%i\n%i\n", next_multiple1, next_multiple2, next_multiple3);
    return 0;
}