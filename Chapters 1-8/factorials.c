#include <stdio.h>

int main(void){
    int temp;
    
    printf(" n        nth factorial\n");
    printf("=======================\n");
    
    for(int i = 1; i <= 10; i++){
       temp = 1;
       for(int j = 1; j <= i; j++){
           temp = temp * j;
       } 
       printf("%2i        %8i\n", i, temp);
    }
    return 0;
}