#include <stdio.h>

int main(void){
    int temp, number, result;
    
    printf("Enter integer to sum: \n");
    scanf("%i", &number);
    
    result = 0;
    do{
       temp = number % 10;
       result += temp;
       number = number / 10;
    } while(number != 0);
   
    printf("Result is: %i\n", result);
    
    return 0;
}