#include <stdio.h>

int main(void){
    
    float floats[10] = {10.43, 4.54, 8.356, 76.23, 6.358, 765.2, 54.4, 8.1, 32.765, 10};
    int i;
    float total;
    
    for(i = 0; i < 10; ++i){
        total += floats[i];
    }
    
    printf("Average: %.3f\n", total / 10);
    return 0;
}