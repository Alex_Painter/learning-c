#include <stdio.h>
#include <math.h>

int main(void){
    printf(" n           n(2)\n");
    printf("=================\n");
    
    int j;
    for(int i = 1; i <= 10; ++i){
       j = pow(i, 2);
       printf("%2i          %3i\n", i, j);
    }
    
    return 0;
}