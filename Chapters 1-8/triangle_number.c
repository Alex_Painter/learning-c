#include <stdio.h>

int main(void){
    //triangularNumber = n (n + 1) / 2 
    int triangle_number;
    
    printf(" n         t-number\n");
    printf("===================\n");
    
    for(int i = 5; i <= 50; i += 5){
       triangle_number = i * (i + 1) / 2;
       printf("%2i           %4i\n", i, triangle_number);
    }
    
    return 0;
}