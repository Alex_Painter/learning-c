#include <stdio.h>
#include <math.h>

int main(void){
    //3x^3 - 5x^2 + 6
    // x = 2.55
    
    long double x = 2.55f;
    
    long double poly = (3 * pow(x, 3)) - (5 * pow(x, 2)) + 6;
    
    printf("Polynomial result: %Lf\n", poly);
    
    return 0;
}