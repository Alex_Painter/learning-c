#include <stdio.h>

int main(void){
    
    float value1, value2;
    
    printf("Enter two numbers to dvide with 3 decimal places: \n");
    scanf("%f%f", &value1, &value2);
    
    if(value2 == 0){
        printf("Dividing by 0 you fool!\n");
    }else{
        printf("Result: %.3f\n", (value1 / value2));
    }
    return 0;
}