//broken if you enter anything but a float as first variable
#include <stdio.h>
#include <stdlib.h>

int main(void){

    int scan;
    float acc, temp;
    char operator = ' ';

    printf("Enter number followed by operator. 'S' to set Accumulator, 'E' to end.\n");

    while(operator != 'E'){
        scan = scanf("%f %1c", &temp, &operator); 
        printf("Scan = %d\n", scan);
        if(scan != 2){
            printf("Try again!\n");
            fflush(stdin);
        }else{
            switch(operator){
                case 'E':
                    printf("Exiting...\n");
                    exit(0);
                case 'S':
                    acc = temp;
                    printf("Acc set to %f\n", acc);
                    break;
                case '+':
                    acc += temp;
                    printf("Acc set to %f (+)\n", acc);
                    break;
                case '-':
                    acc -= temp;
                    printf("Acc set to %f (-)\n", acc);
                    break;
                case '*':
                    acc *= temp;
                    printf("Acc set to %f (*)\n", acc);
                    break;
                case '/':
                    if(temp == 0){
                        printf("Dividing by 0!\n");
                    }else{
                        acc /= temp;
                        printf("Acc set to %f (/)\n", acc);
                    }
                    break;
                default:
                    printf("Operator was wrong, try again!\n");
                    break;
            }
        }

    }
    printf("Finished!\n");
    return 0;
}
