#include <stdio.h>

int main(void){
    int number, temp;
    _Bool is_neg = 0;
    
    printf("Enter your number: \n");
    scanf("%i", &number);
    
    if(number < 0){
        number = -(number);
        is_neg = 1;
    }
    
    do {
       temp = number % 10;
       printf("%i", temp);
       number = number / 10;
    }
    while(number != 0);
    
    if(is_neg){
        printf("-");
    }
    
    printf("\n");
    
    return 0;
}