#include <stdio.h>

int main(void){
    
    int fahrenheit = 27;
    float celsius = ((fahrenheit - 32) / 1.8);
    
    printf("Temperature in Celsius is: %f\n", celsius);
    
    return 0;
    //C = (F - 32) / 1.8
}