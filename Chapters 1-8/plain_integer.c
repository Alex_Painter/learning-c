#include <stdio.h>

int main(void){
    
    int number, temp, tens;
    
    printf("Enter a number to turn to plain text: \n");
    scanf("%d", &number);
    
    temp = number;
    tens = 1;
    
    while(temp > 10){
        tens *= 10;
        temp /= 10;
    }
        
    do{
        temp = number / tens;
        switch (temp){
            case 0:
                printf("Zero ");
                break;
            case 1:
                printf("One ");
                break;
            case 2:
                printf("Two ");
                break;
            case 3:
                printf("Three ");
                break;
            case 4:
                printf("Four ");
                break;
            case 5:
                printf("Five ");
                break;
            case 6:
                printf("Six ");
                break;
            case 7:
                printf("Seven ");
                break;
            case 8:
                printf("Eight ");
                break;
            case 9:
                printf("Nine ");
                break;
        }
        number %= tens;
        tens /=10;
    }while(tens >= 1);
    printf("\n");
}